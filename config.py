ATHENA_DB = "sacramento"
ATHENA_TABLE = "crime_data"

S3_BUCKET = "s3://athena-demo-data"
S3_INPUT_DIR = f"{S3_BUCKET}/crime-data/"
S3_OUTPUT_DIR = f"{S3_BUCKET}/query-results/"
