from config import ATHENA_DB, ATHENA_TABLE, S3_INPUT_DIR

FIELD_DELIMITER = ","

# Athena supports PrestoDB SQL syntax: https://prestodb.io/docs/current/index.html
CONVERTED_DATETIME = "date_parse(ts, '%c/%e/%y %k:%i')"
LINE_DELIMITER = "\n"

CREATE_DATABASE = f"CREATE DATABASE IF NOT EXISTS {ATHENA_DB};"

CREATE_TABLE = f"CREATE EXTERNAL TABLE IF NOT EXISTS {ATHENA_DB}.{ATHENA_TABLE} (" + \
               "ts VARCHAR(255), " \
               "address VARCHAR(255), " \
               "district INT, " \
               "beat VARCHAR(255), " \
               "grid INT, " \
               "crimedescr VARCHAR(255), " \
               "ucr_ncic_code INT, " \
               "latitude FLOAT, " \
               "longitude FLOAT" \
               ") " \
               "ROW FORMAT DELIMITED " \
               f"FIELDS TERMINATED BY '{FIELD_DELIMITER}' " \
               f"LINES TERMINATED BY '{LINE_DELIMITER}' " \
               f"LOCATION '{S3_INPUT_DIR}' " \
               "TBLPROPERTIES (" \
               "'skip.header.line.count' = '1'" \
               ");"

DROP_DATABASE = f"DROP DATABASE {ATHENA_DB};"

DROP_TABLE = f"DROP TABLE {ATHENA_DB}.{ATHENA_TABLE};"

# Initial clause for WHERE added to circumvent erroneous CSV header parsing from Athena
SELECT_RECORDS_WITHIN_INTERVAL = "SELECT * " \
                                 f"FROM {ATHENA_DB}.{ATHENA_TABLE} " \
                                 f"WHERE {CONVERTED_DATETIME} >= timestamp '{{}}' " \
                                 f"AND {CONVERTED_DATETIME} <= timestamp '{{}}';"
