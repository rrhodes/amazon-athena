import os
import time

import boto3
from botocore import exceptions

import queries
from config import S3_OUTPUT_DIR

END_DATE = "2006-01-02"
MAX_ATTEMPTS = 3
START_DATE = "2006-01-01"


def handle_invalid_mfa_code(mfa_code_attempt_count: int) -> int:
    mfa_code_attempt_count += 1

    if mfa_code_attempt_count != MAX_ATTEMPTS:
        print('Invalid MFA one-time passcode provided. %s attempt(s) remaining. Please try again.'
              % (MAX_ATTEMPTS - mfa_code_attempt_count))
    else:
        print('Invalid MFA one-time passcode provided.')

    return mfa_code_attempt_count


def create_session() -> boto3.Session:
    sts_client = boto3.client('sts')

    invalid_mfa_code = True
    mfa_code_attempt_count = 0

    while invalid_mfa_code and mfa_code_attempt_count < 3:
        mfa_code = input('Enter your current MFA code: ')

        if len(mfa_code) != 6 or not mfa_code.isdigit():
            mfa_code_attempt_count = handle_invalid_mfa_code(mfa_code_attempt_count)
        else:
            mfa_serial_number = os.getenv('AWS_MFA_SERIAL_NUMBER')
            try:
                sts_client.get_session_token(
                    DurationSeconds=3600,
                    SerialNumber=mfa_serial_number,
                    TokenCode=mfa_code
                )
                invalid_mfa_code = False
            except exceptions.ClientError:
                mfa_code_attempt_count = handle_invalid_mfa_code(mfa_code_attempt_count)

    if invalid_mfa_code:
        exit("No valid MFA one-time passcode provided.")

    return boto3.Session()


def execute_query(athena_client: boto3.client, query: str) -> dict:
    return athena_client.start_query_execution(
        QueryString=query,
        ResultConfiguration={
            "OutputLocation": S3_OUTPUT_DIR,
            "EncryptionConfiguration": {
                "EncryptionOption": "SSE_S3"
            }
        }
    )


def fetch_query_results(athena_client: boto3.client, amazon_response: dict) -> dict:
    query_result = None
    num_attempts = 0

    while not query_result and num_attempts < MAX_ATTEMPTS:
        time.sleep(3)
        num_attempts += 1

        try:
            query_result = athena_client.get_query_results(
                QueryExecutionId=amazon_response['QueryExecutionId']
            )
        except exceptions.ClientError:
            print(f"Attempt {num_attempts} of {MAX_ATTEMPTS} failed.")
            if num_attempts < MAX_ATTEMPTS:
                print("Trying again ...")
            else:
                print("No further retries.")

    return query_result


def main():
    session = create_session()

    if not session:
        exit("No boto3 session created.")

    athena_client = boto3.client('athena', region_name='eu-west-1')

    try:
        execute_query(athena_client, queries.CREATE_DATABASE)
        execute_query(athena_client, queries.CREATE_TABLE)

        select_records_in_february = queries.SELECT_RECORDS_WITHIN_INTERVAL.format(START_DATE, END_DATE)
        amazon_response = execute_query(athena_client, select_records_in_february)

        query_results = fetch_query_results(athena_client, amazon_response)
        result_row_count = len(query_results['ResultSet']['Rows'])

        print(f"Number of crimes between {START_DATE} and {END_DATE}: {result_row_count}")

        execute_query(athena_client, queries.DROP_TABLE)
        execute_query(athena_client, queries.DROP_DATABASE)
    except exceptions.ClientError as err:
        exit(err.response['Error']['Message'])

    exit(0)


if __name__ == '__main__':
    main()